Weather station developed using Arduino to measure in real time:

- date
- time
- temperature
- pressure
- humidity
- air quality

and save data to SD card (external memory).
