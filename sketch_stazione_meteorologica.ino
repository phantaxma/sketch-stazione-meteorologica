/*
   Stazione Meteorologica
   ATD - progetto finale

   I2C conections:
   LCD             ->  Pin  : A4 (SDA), A5 (SCL)
   BME280          ->  Pin  : A4 (SDA), A5 (SCL)
   DS1307          ->  Pin  : A4 (SDA), A5 (SCL)

   Analog connections
   MQ135           ->  Pin  : A3, D0 (NC)

   Micro SD Module
 *    * SD card attached to SPI bus as follows:
 *    * MOSI - pin 11
 *    * MISO - pin 12
 *    * CLK  - pin 13
 *    * CS -   pin 4
*/
#include <SPI.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Adafruit_Sensor.h>
#include "RTClib.h"
#include <Adafruit_BME280.h>
#include <SD.h>

/*defines*/
//LCD
#define LCD_I2C_ADDRESS 0x3F
#define LCD_I2C_RS 0
#define LCD_I2C_RW 1
#define LCD_I2C_EN 2
#define LCD_I2C_D4 4
#define LCD_I2C_D5 5
#define LCD_I2C_D6 6
#define LCD_I2C_D7 7
#define LCD_I2C_BL 3

#define LCD_ROWS 4
#define LCD_COLUMNS 20
#define SCROLL_DELAY 150
#define BACKLIGHT 255

//BME280
#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10
#define SEALEVELPRESSURE_HPA (1013.25)
#define BME_ADDRESS 0x76

//DS1307
//#define DS1307_ADDRESS

//MQ135
#define MQ135_A0 A3

/*global variables*/
//Micro SD Module
const int chipSelect = 4;
File dataFile;
const String fileName = "datalog.txt";

//LCD
LiquidCrystal_I2C lcd(LCD_I2C_ADDRESS, LCD_ROWS, LCD_COLUMNS);

//BME280
Adafruit_BME280 bme; // I2C
//Adafruit_BME280 bme(BME_CS); // hardware SPI
//Adafruit_BME280 bme(BME_CS, BME_MOSI, BME_MISO, BME_SCK); // software SPI

//DS1307
RTC_DS1307 rtc;
DateTime now;

//MQ135
//int gasSensorValue = 0;

const unsigned long int interval = 600000; //5 minutes
//const unsigned long int interval = 60000; //1 minutes
//const unsigned long int interval = 10000; // 10 seconds
unsigned long previousMillis = 0;
bool firstMessure = true;

struct DataSensors {
  String date;
  String time;
  float temperature;
  float pressure;
  float humidity;
  int airQuality;
};

DataSensors data;

void setup()
{
  delay(1000);

  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  //LCD
  lcd.begin();
  lcd.backlight();
  //lcd.noBacklight();

  //Micro SD Module
  // make sure that the default chip select pin is set to
  pinMode(10, OUTPUT);
  // output, even if you don't use it:

  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    lcd.setCursor(19, 0);
    lcd.print("!");
    // don't do anything more:
    //return;
  }


  //BME280
  // default settings
  // (you can also pass in a Wire library object like &Wire2)
  if (!bme.begin(BME_ADDRESS)) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    //while (1);
  }

  //DS1307
  if (!rtc.begin())
  {
    lcd.print("Couldn't find RTC");
    //while (1);
  }

  if (!rtc.isrunning())
  {
    lcd.print("RTC is NOT running!");
  }

  rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));//auto update from computer time
  //rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));// to set the time manualy
}

void loop()
{
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval || firstMessure) {
    firstMessure = false;
    previousMillis = currentMillis;

    if (!SD.exists(fileName)) {
      if (!SD.begin(chipSelect)) {
        Serial.println("Card failed, or not present");
        lcd.setCursor(19, 0);
        lcd.print("!");
        // don't do anything more:
        //return;
      }
    }

    collectData();
    lcdPrint();
    serialPrint();
    saveToSD();
  }
}


void collectData()
{
  //DS1307
  now = rtc.now();

  //clear data
  data.date = "";
  data.time = "";
  data.temperature = 0.0;
  data.pressure = 0.0;
  data.humidity = 0.0;
  data.airQuality = 0;

  //collect values
  data.date = String(now.year()) + "-" + String(now.month()) + "-" + String(now.day());
  data.time = String(now.hour()) + ":" + String(now.minute());
  data.temperature = bme.readTemperature();
  data.pressure = bme.readPressure();
  data.humidity = bme.readHumidity();
  data.airQuality = analogRead(MQ135_A0);
}

void serialPrint()
{
  //DS1307
  Serial.print(data.date);
  Serial.print(" ");
  Serial.print(data.time);
  Serial.println();

  //BME280
  Serial.print("T=");
  Serial.print(data.temperature);
  Serial.println("°C");

  Serial.print("P=");
  Serial.print(data.pressure);
  Serial.println();
  Serial.print("h=");
  Serial.print(data.humidity);
  Serial.print("%");
  Serial.println();

  //MQ135
  Serial.print("ppm=");
  Serial.print(data.airQuality);
  Serial.println();
}

void lcdPrint()
{
  //DS1307
  lcd.setCursor(0, 0);
  lcd.print(data.time);

  lcd.setCursor(8, 0);
  lcd.print(data.date);

  //BME280
  lcd.setCursor(0, 2);
  lcd.print("T=");
  lcd.setCursor(2, 2);
  lcd.print(data.temperature);
  lcd.setCursor(7, 2);
  lcd.print("\xDF");
  //lcd.setCursor(7, 2);
  lcd.print("C");

  lcd.setCursor(0, 1);
  lcd.print("P=");
  lcd.setCursor(2, 1);
  lcd.print(data.pressure);
  lcd.setCursor(10, 1);
  lcd.print("Pa");
  lcd.setCursor(11, 2);
  lcd.print("h=");
  lcd.setCursor(13, 2);
  lcd.print(data.humidity);
  lcd.setCursor(18, 2);
  lcd.print("%");

  //MQ135
  lcd.setCursor(7, 3);
  lcd.print("ppm=");
  lcd.setCursor(11, 3);
  lcd.print(data.airQuality);
}

void saveToSD()
{
  // make a string for assembling the data to log:
  String dataString = "";

  // append data to the string:
  dataString += data.date + "\t" + data.time + "\t";
  dataString += String(data.pressure) + "\t";
  dataString += String(data.temperature) + "\t";
  dataString += String(data.humidity) + "\t";
  dataString += String(data.airQuality);

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  //date  time  pressure  temperature  humidity  air-quality
  dataFile = SD.open(fileName, FILE_WRITE);

  // if the file is available, write to it:
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    // print to the serial port too:
    Serial.println(dataString);
    lcd.setCursor(19, 0);
    lcd.print(" ");
  }
  // if the file isn't open, pop up an error:
  else {
    Serial.println("error opening datalog.txt");
    lcd.setCursor(19, 0);
    lcd.print("!");
  }

}
